package com.architecture.ddd.adapters.in.web;

import com.architecture.ddd.adapters.in.web.controller.FetchHospitalController;
import com.architecture.ddd.application.port.in.FetchHospitalUseCase;
import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;
import java.util.Set;

import static com.architecture.ddd.adapters.in.web.config.WebApplicationConfiguration.API_V1_HOSPITAL_URL;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FetchHospitalController.class)
@AutoConfigureMockMvc
class FetchHospitalControllerShould {

  @MockBean private FetchHospitalUseCase fetchHospitalUseCase;

  @Autowired private MockMvc mockMvc;

  @Test
  void getHospitalWhenTheHospitalExists() throws Exception {

    final String HOSPITAL_NAME = "testHospitalName";

    // given
    given(fetchHospitalUseCase.getHospitalByName(HOSPITAL_NAME)).willReturn(createTestHospital());

    // when
    mockMvc
        .perform(
            get(API_V1_HOSPITAL_URL + "/{hospitalId}", HOSPITAL_NAME)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
        // then
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", is("Hospital successfully retrieved.")))
        .andExpect(jsonPath("$.status", is("OK")))
        .andExpect(jsonPath("$.timestamp").isNotEmpty());

    then(fetchHospitalUseCase).should().getHospitalByName(HOSPITAL_NAME);
  }

  @Test
  void returnAnErrorResponseWhenTheHospitalDoesNotExist() throws Exception {

    final String HOSPITAL_NAME = "testHospitalName";

    // given
    given(fetchHospitalUseCase.getHospitalByName(HOSPITAL_NAME))
        .willThrow(
            new EntityNotFoundException(
                "The Hospital with name: " + HOSPITAL_NAME + " was not found."));
    // when
    mockMvc
        .perform(
            get(API_V1_HOSPITAL_URL + "/{hospitalId}", HOSPITAL_NAME)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
        // then
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.message", is("Unable to retrieve entity.")))
        .andExpect(jsonPath("$.status", is("NOT_FOUND")))
        .andExpect(jsonPath("$.timestamp").isNotEmpty());
  }

  private Hospital createTestHospital() {
    return Hospital.builder()
        .name("testHospitalName")
        .totalNumberOfBeds(15)
        .address("testHospitalAddress")
        .telephoneNumber("6942637445")
        .hospitalUsersIds(Set.of())
        .build();
  }
}
