package com.architecture.ddd.adapters.out.persistence;

import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;

import javax.persistence.EntityNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@DataJpaTest
@Import({HospitalOutPersistenceAdapter.class, HospitalMapper.class})
class HospitalOutPersistenceAdapterShould {

  @Autowired private TestEntityManager testEntityManager;

  @Autowired private HospitalOutPersistenceAdapter adapterUnderTest;

  @BeforeEach
  void setUp() {
    // given
    HospitalJpaEntity hospitalJpaEntity =
        HospitalJpaEntity.builder()
            .name("validHospitalName")
            .address("testAddress")
            .telephoneNumber("692912939129")
            .totalNumberOfBeds(15)
            .build();

    testEntityManager.merge(hospitalJpaEntity);
  }

  @Test
  void loadHospitalWhenTheHospitalExists() {

    // when
    Hospital hospital = adapterUnderTest.loadHospitalByName("validHospitalName");

    // then
    assertThat(hospital.getAddress()).isEqualTo("testAddress");
    assertThat(hospital.getTelephoneNumber()).isEqualTo("692912939129");
    assertThat(hospital.getTotalNumberOfBeds()).isEqualTo(15);
    assertThat(hospital.getId()).isNotNull();
    assertThat(hospital.getHospitalUsersIds()).isEmpty();
  }

  @Test
  void throwExceptionWhenTheHospitalDoesNotExist() {

    // when
    Throwable thrown =
        catchThrowable(() -> adapterUnderTest.loadHospitalByName("invalidHospitalName"));

    // then
    assertThat(thrown)
        .isInstanceOf(EntityNotFoundException.class)
        .hasMessage("The Hospital with name: " + "invalidHospitalName" + " was not found.");
  }
}
