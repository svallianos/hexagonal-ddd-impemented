package com.architecture.ddd.adapters.out.persistence;

import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class HospitalMapperShould {

  private HospitalMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new HospitalMapper();
  }

  @Test
  void mapHospitalToJpaEntity() {
    // given
    Hospital hospital =
        Hospital.builder()
            .address("testAddress")
            .totalNumberOfBeds(15)
            .telephoneNumber("688128312")
            .name("testName")
            .hospitalUsersIds(Set.of())
            .build();

    // when
    HospitalJpaEntity hospitalJpaEntity = mapper.mapToJpaEntity(hospital);

    // then
    assertThat(hospitalJpaEntity.getHospitalUsersIds()).isEqualTo(Set.of());
    assertThat(hospitalJpaEntity.getAddress()).isEqualTo(hospital.getAddress());
    assertThat(hospitalJpaEntity.getId()).isNull();
    assertThat(hospitalJpaEntity.getOrganizationType()).isEqualTo(OrganizationType.HOSPITAL);
    assertThat(hospitalJpaEntity.getName()).isEqualTo(hospital.getName());
    assertThat(hospitalJpaEntity.getTelephoneNumber()).isEqualTo(hospital.getTelephoneNumber());
  }

  @Test
  void mapJpaEntityToDomainEntity() {
    // given

    final UUID HOSPITAL_ID = UUID.randomUUID();

    HospitalJpaEntity hospitalJpaEntity =
        HospitalJpaEntity.builder()
            .id(HOSPITAL_ID)
            .address("testAddress")
            .totalNumberOfBeds(15)
            .telephoneNumber("688128312")
            .name("testName")
            .createdDate(LocalDateTime.now())
            .lastModifiedDate(LocalDateTime.now())
            .hospitalUsersIds(Stream.of(1L).collect(Collectors.toSet()))
            .build();

    // when
    Hospital hospital = mapper.mapToDomainEntity(hospitalJpaEntity);

    // then
    assertThat(hospital.getHospitalUsersIds()).isEqualTo(hospitalJpaEntity.getHospitalUsersIds());
    assertThat(hospital.getAddress()).isEqualTo(hospitalJpaEntity.getAddress());
    assertThat(hospital.getId()).isEqualTo(hospitalJpaEntity.getId());
    assertThat(hospital.getName()).isEqualTo(hospitalJpaEntity.getName());
    assertThat(hospital.getTelephoneNumber()).isEqualTo(hospitalJpaEntity.getTelephoneNumber());
    assertThat(hospital.getTotalNumberOfBeds()).isEqualTo(hospitalJpaEntity.getTotalNumberOfBeds());
  }
}
