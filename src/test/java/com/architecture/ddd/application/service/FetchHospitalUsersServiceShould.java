package com.architecture.ddd.application.service;

import com.architecture.ddd.application.port.out.LoadHospitalOutPort;
import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class FetchHospitalUsersServiceShould {

  @Mock private LoadHospitalOutPort loadHospitalOutPort;

  @InjectMocks private FetchHospitalUsersService fetchHospitalUsersService;

  @Test
  void fetchHospitalUsersIfTheHospitalExists() {

    final String HOSPITAL_NAME = "testHospitalName";
    Set<Long> expectedHospitalUsers = Set.of(1L);

    // given
    given(loadHospitalOutPort.loadHospitalByName(HOSPITAL_NAME)).willReturn(createTestHospital());

    // when
    Set<Long> actualRetrievedUsers = fetchHospitalUsersService.fetchByHospitalName(HOSPITAL_NAME);

    // then
    assertThat(actualRetrievedUsers).isEqualTo(expectedHospitalUsers);
    then(loadHospitalOutPort).should().loadHospitalByName(HOSPITAL_NAME);
  }

  @Test
  void throwAnExceptionIfTheHospitalDoesNotExist() {

    final String HOSPITAL_NAME = "testHospitalName";

    // given
    given(loadHospitalOutPort.loadHospitalByName(HOSPITAL_NAME))
        .willThrow(
            new EntityNotFoundException(
                "The Hospital with name: " + HOSPITAL_NAME + " was not found."));
    // when
    Throwable thrown =
        catchThrowable(() -> fetchHospitalUsersService.fetchByHospitalName(HOSPITAL_NAME));
    // then
    assertThat(thrown)
        .isInstanceOf(EntityNotFoundException.class)
        .hasMessage("The Hospital with name: " + HOSPITAL_NAME + " was not found.");
  }

  private Hospital createTestHospital() {
    return Hospital.builder()
        .name("testHospitalName")
        .totalNumberOfBeds(15)
        .address("testHospitalAddress")
        .telephoneNumber("6942637445")
        .hospitalUsersIds(Set.of(1L))
        .build();
  }
}
