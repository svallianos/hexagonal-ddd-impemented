package com.architecture.ddd.application.service;

import com.architecture.ddd.application.port.out.PersistHospitalOutPort;
import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static com.architecture.ddd.application.port.in.RegisterHospitalUseCase.RegisterHospitalCommand;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class RegisterHospitalServiceShould {

  @Mock private PersistHospitalOutPort persistHospitalOutPort;
  @Mock private RegisterHospitalCommandMapper registerHospitalCommandMapper;

  @InjectMocks private RegisterHospitalService registerHospitalService;

  @BeforeEach
  void setUp() {}

  @Test
  void registerHospital() {

    Hospital testHospital = createTestHospital();
    RegisterHospitalCommand testCommand = createTestRegisterHospitalCommand();

    // given
    given(persistHospitalOutPort.persistHospital(testHospital)).willReturn(testHospital);
    given(registerHospitalCommandMapper.mapToDomainEntity(testCommand)).willReturn(testHospital);

    // when
    registerHospitalService.registerHospital(testCommand);

    // then
    then(persistHospitalOutPort).should().persistHospital(testHospital);
    then(registerHospitalCommandMapper).should().mapToDomainEntity(testCommand);
  }

  private Hospital createTestHospital() {
    return Hospital.builder()
        .name("testHospitalName")
        .totalNumberOfBeds(15)
        .address("testHospitalAddress")
        .telephoneNumber("6942637445")
        .hospitalUsersIds(Set.of(1L))
        .build();
  }

  private RegisterHospitalCommand createTestRegisterHospitalCommand() {
    return RegisterHospitalCommand.builder()
        .name("testHospitalName")
        .totalNumberOfBeds(15)
        .address("testHospitalAddress")
        .telephoneNumber("6942637445")
        .build();
  }
}
