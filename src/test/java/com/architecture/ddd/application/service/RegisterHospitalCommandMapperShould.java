package com.architecture.ddd.application.service;

import com.architecture.ddd.domain.Hospital;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.architecture.ddd.application.port.in.RegisterHospitalUseCase.RegisterHospitalCommand;
import static org.assertj.core.api.Assertions.assertThat;

class RegisterHospitalCommandMapperShould {

  private RegisterHospitalCommandMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new RegisterHospitalCommandMapper();
  }

  @Test
  void mapRegisterHospitalCommandToDomainEntity() {

    // given
    RegisterHospitalCommand command = createTestRegisterHospitalCommand();

    // when
    Hospital mappedHospital = mapper.mapToDomainEntity(command);

    // then
    assertThat(mappedHospital.getTotalNumberOfBeds()).isEqualTo(command.getTotalNumberOfBeds());
    assertThat(mappedHospital.getTelephoneNumber()).isEqualTo(command.getTelephoneNumber());
    assertThat(mappedHospital.getAddress()).isEqualTo(command.getAddress());
    assertThat(mappedHospital.getName()).isEqualTo(command.getName());
    assertThat(mappedHospital.getHospitalUsersIds()).isEmpty();
    assertThat(mappedHospital.getId()).isNull();
  }

  private RegisterHospitalCommand createTestRegisterHospitalCommand() {
    return RegisterHospitalCommand.builder()
        .name("testHospitalName")
        .totalNumberOfBeds(15)
        .address("testHospitalAddress")
        .telephoneNumber("6942637445")
        .build();
  }
}
