package com.architecture.ddd.application.service;

import com.architecture.ddd.application.port.in.FetchHospitalUsersUseCase;
import com.architecture.ddd.application.port.out.LoadHospitalOutPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class FetchHospitalUsersService implements FetchHospitalUsersUseCase {

  private final LoadHospitalOutPort loadHospitalOutPort;

  @Override
  public Set<Long> fetchByHospitalName(String hospitalName) {
    return loadHospitalOutPort.loadHospitalByName(hospitalName).getHospitalUsersIds();
  }
}
