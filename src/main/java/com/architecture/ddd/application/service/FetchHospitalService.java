package com.architecture.ddd.application.service;

import com.architecture.ddd.application.port.in.FetchHospitalUseCase;
import com.architecture.ddd.application.port.out.LoadHospitalOutPort;
import com.architecture.ddd.domain.Hospital;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class FetchHospitalService implements FetchHospitalUseCase {

  private final LoadHospitalOutPort loadHospitalOutPort;

  @Override
  public Hospital getHospitalByName(String hospitalName) {
    return loadHospitalOutPort.loadHospitalByName(hospitalName);
  }
}
