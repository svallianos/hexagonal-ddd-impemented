package com.architecture.ddd.application.service;

import com.architecture.ddd.domain.Hospital;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.architecture.ddd.application.port.in.RegisterHospitalUseCase.RegisterHospitalCommand;

@Component
@Slf4j
class RegisterHospitalCommandMapper {

  Hospital mapToDomainEntity(RegisterHospitalCommand command) {

    return Hospital.builder()
        .address(command.getAddress())
        .name(command.getName())
        .totalNumberOfBeds(command.getTotalNumberOfBeds())
        .telephoneNumber(command.getTelephoneNumber())
        .totalNumberOfBeds(command.getTotalNumberOfBeds())
        .hospitalUsersIds(Set.of())
        .build();
  }
}
