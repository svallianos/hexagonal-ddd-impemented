package com.architecture.ddd.application.service;

import com.architecture.ddd.application.port.in.RegisterHospitalUseCase;
import com.architecture.ddd.application.port.out.PersistHospitalOutPort;
import com.architecture.ddd.domain.Hospital;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@RequiredArgsConstructor
@Service
@Transactional
public class RegisterHospitalService implements RegisterHospitalUseCase {

  private final PersistHospitalOutPort persistHospitalOutPort;
  private final RegisterHospitalCommandMapper registerHospitalCommandMapper;

  @Override
  public Hospital registerHospital(RegisterHospitalCommand command) {

    return persistHospitalOutPort.persistHospital(
        registerHospitalCommandMapper.mapToDomainEntity(command));
  }
}
