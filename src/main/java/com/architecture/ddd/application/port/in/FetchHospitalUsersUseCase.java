package com.architecture.ddd.application.port.in;

import java.util.Set;

public interface FetchHospitalUsersUseCase {

  Set<Long> fetchByHospitalName(String hospitalName);
}
