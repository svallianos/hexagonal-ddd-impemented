package com.architecture.ddd.application.port.out;

import com.architecture.ddd.domain.Hospital;

public interface LoadHospitalOutPort {

  Hospital loadHospitalByName(String hospitalName);
}
