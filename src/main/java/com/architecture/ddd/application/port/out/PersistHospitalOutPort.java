package com.architecture.ddd.application.port.out;

import com.architecture.ddd.domain.Hospital;

public interface PersistHospitalOutPort {

  Hospital persistHospital(Hospital hospital);
}
