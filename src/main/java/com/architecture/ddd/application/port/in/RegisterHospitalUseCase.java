package com.architecture.ddd.application.port.in;

import com.architecture.ddd.domain.Hospital;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public interface RegisterHospitalUseCase {

  Hospital registerHospital(RegisterHospitalCommand command);

  @Value
  @Builder
  class RegisterHospitalCommand {

    @Size(min = 2, max = 200)
    @NotBlank
    @NonNull
    private final String name;

    @Size(min = 2, max = 200)
    @NotBlank
    @NonNull
    private final String address;

    @Size(min = 2, max = 200)
    @NotBlank
    @NonNull
    private final String telephoneNumber;

    @NotNull @NonNull private final Integer totalNumberOfBeds;
  }
}
