package com.architecture.ddd.application.port.in;

import com.architecture.ddd.domain.Hospital;

public interface FetchHospitalUseCase {

  Hospital getHospitalByName(String hospitalName);
}
