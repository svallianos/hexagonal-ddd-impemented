package com.architecture.ddd.adapters.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
interface HospitalRepository extends JpaRepository<HospitalJpaEntity, UUID> {
  Optional<HospitalJpaEntity> findByName(String hospitalName);
}
