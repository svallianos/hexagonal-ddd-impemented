package com.architecture.ddd.adapters.out.persistence;

import com.architecture.ddd.domain.Hospital;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
class HospitalMapper {

  HospitalJpaEntity mapToJpaEntity(Hospital hospital) {

    return HospitalJpaEntity.builder()
        .address(hospital.getAddress())
        .name(hospital.getName())
        .telephoneNumber(hospital.getTelephoneNumber())
        .totalNumberOfBeds(hospital.getTotalNumberOfBeds())
        .build();
  }

  Hospital mapToDomainEntity(HospitalJpaEntity hospitalJpaEntity) {

    return Hospital.builder()
        .name(hospitalJpaEntity.getName())
        .address(hospitalJpaEntity.getAddress())
        .telephoneNumber(hospitalJpaEntity.getTelephoneNumber())
        .totalNumberOfBeds(hospitalJpaEntity.getTotalNumberOfBeds())
        .id(hospitalJpaEntity.getId())
        .hospitalUsersIds(hospitalJpaEntity.getHospitalUsersIds())
        .createdDate(hospitalJpaEntity.getCreatedDate())
        .lastModifiedDate(hospitalJpaEntity.getLastModifiedDate())
        .build();
  }

  private Set<Long> setHospitalUsersIds(Hospital hospital) {

    if (hospital.getHospitalUsersIds() == null) {
      return Set.of();

    } else return hospital.getHospitalUsersIds();
  }
}
