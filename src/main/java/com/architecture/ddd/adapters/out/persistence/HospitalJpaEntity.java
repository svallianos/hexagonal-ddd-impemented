package com.architecture.ddd.adapters.out.persistence;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hospital")
@EntityListeners(AuditingEntityListener.class)
@Builder
class HospitalJpaEntity {

  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "hospital_id", updatable = false, nullable = false, length = 50)
  private UUID id;

  @Enumerated(EnumType.STRING)
  @Column(updatable = false)
  @Builder.Default
  private OrganizationType organizationType = OrganizationType.HOSPITAL;

  @CreatedDate
  @Column(updatable = false)
  private LocalDateTime createdDate;

  @LastModifiedDate private LocalDateTime lastModifiedDate;

  @NonNull private String name;
  @NonNull private String address;
  @NonNull private String telephoneNumber;
  @NonNull private Integer totalNumberOfBeds;

  @ElementCollection @Builder.Default @NonNull Set<Long> hospitalUsersIds = new HashSet<>();
}
