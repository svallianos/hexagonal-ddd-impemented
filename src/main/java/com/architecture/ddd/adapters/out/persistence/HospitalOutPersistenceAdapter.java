package com.architecture.ddd.adapters.out.persistence;

import com.architecture.ddd.application.port.out.LoadHospitalOutPort;
import com.architecture.ddd.application.port.out.PersistHospitalOutPort;
import com.architecture.ddd.domain.Hospital;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
@RequiredArgsConstructor
class HospitalOutPersistenceAdapter implements PersistHospitalOutPort, LoadHospitalOutPort {

  private final HospitalRepository hospitalRepository;
  private final HospitalMapper hospitalMapper;

  @Override
  public Hospital persistHospital(Hospital hospital) {

    return hospitalMapper.mapToDomainEntity(
        hospitalRepository.save(hospitalMapper.mapToJpaEntity(hospital)));
  }

  @Override
  public Hospital loadHospitalByName(String hospitalName) {

    HospitalJpaEntity retrievedHospital =
        hospitalRepository
            .findByName(hospitalName)
            .orElseThrow(
                () ->
                    new EntityNotFoundException(
                        "The Hospital with name: " + hospitalName + " was not found."));

    return hospitalMapper.mapToDomainEntity(retrievedHospital);
  }
}
