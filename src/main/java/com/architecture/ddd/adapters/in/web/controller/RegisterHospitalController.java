package com.architecture.ddd.adapters.in.web.controller;

import com.architecture.ddd.adapters.in.web.common.ApiSuccessResponse;
import com.architecture.ddd.application.port.in.RegisterHospitalUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.architecture.ddd.adapters.in.web.config.WebApplicationConfiguration.API_V1_HOSPITAL_URL;
import static com.architecture.ddd.application.port.in.RegisterHospitalUseCase.RegisterHospitalCommand;

@RestController
@RequestMapping(API_V1_HOSPITAL_URL)
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class RegisterHospitalController {

  private final RegisterHospitalUseCase registerHospitalUseCase;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ApiSuccessResponse registerHospital(@Valid @RequestBody RegisterHospitalCommand command) {

    return ApiSuccessResponse.builder()
        .status(HttpStatus.CREATED)
        .data(registerHospitalUseCase.registerHospital(command))
        .message("Hospital successfully registered.")
        .build();
  }
}
