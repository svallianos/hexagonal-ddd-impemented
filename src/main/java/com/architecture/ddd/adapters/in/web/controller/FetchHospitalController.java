package com.architecture.ddd.adapters.in.web.controller;

import com.architecture.ddd.adapters.in.web.common.ApiSuccessResponse;
import com.architecture.ddd.application.port.in.FetchHospitalUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.architecture.ddd.adapters.in.web.config.WebApplicationConfiguration.*;

@RestController
@RequestMapping(API_V1_HOSPITAL_URL)
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class FetchHospitalController {

  private final FetchHospitalUseCase fetchHospitalUseCase;

  @GetMapping("/{hospitalName}")
  @ResponseStatus(HttpStatus.OK)
  public ApiSuccessResponse fetchHospital(@PathVariable String hospitalName) {

    return ApiSuccessResponse.builder()
        .status(HttpStatus.OK)
        .data(fetchHospitalUseCase.getHospitalByName(hospitalName))
        .message("Hospital successfully retrieved.")
        .build();
  }
}
