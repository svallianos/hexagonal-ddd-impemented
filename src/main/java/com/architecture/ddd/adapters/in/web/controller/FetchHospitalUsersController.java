package com.architecture.ddd.adapters.in.web.controller;

import com.architecture.ddd.adapters.in.web.common.ApiSuccessResponse;
import com.architecture.ddd.application.port.in.FetchHospitalUsersUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.architecture.ddd.adapters.in.web.config.WebApplicationConfiguration.API_V1_HOSPITAL_URL;
import static com.architecture.ddd.adapters.in.web.config.WebApplicationConfiguration.USERS_URL;

@RestController
@RequestMapping(API_V1_HOSPITAL_URL)
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class FetchHospitalUsersController {

  private final FetchHospitalUsersUseCase fetchHospitalUsersUseCase;

  @GetMapping(USERS_URL + "/{hospitalName}")
  @ResponseStatus(HttpStatus.OK)
  public ApiSuccessResponse fetchHospitalUsers(@PathVariable String hospitalName) {

    return ApiSuccessResponse.builder()
        .status(HttpStatus.OK)
        .data(fetchHospitalUsersUseCase.fetchByHospitalName(hospitalName))
        .message("Hospital Users successfully retrieved.")
        .build();
  }
}
