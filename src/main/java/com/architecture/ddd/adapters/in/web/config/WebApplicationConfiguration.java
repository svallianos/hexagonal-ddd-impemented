package com.architecture.ddd.adapters.in.web.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class WebApplicationConfiguration {

  public static final String API_V1_HOSPITAL_URL = "/api/v1/hospitals";
  public static final String USERS_URL = "/users";
}
