package com.architecture.ddd.adapters.in.web.common.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

abstract class ApiSubErrorMessage {}

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
class ApiValidationErrorMessage extends ApiSubErrorMessage {
  private String field;
  private Object rejectedValue;
  private String message;

  ApiValidationErrorMessage(String message) {
    this.message = message;
  }
}
