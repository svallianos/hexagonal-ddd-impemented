package com.architecture.ddd.adapters.in.web.common.exceptions.customexceptions;

public class OrganizationAlreadyExistsException extends RuntimeException {
  public OrganizationAlreadyExistsException(String name) {
    super("The organization with name: " + name + " already exists.");
  }
}
