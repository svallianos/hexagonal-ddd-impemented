package com.architecture.ddd.domain;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Value
@Builder
public class Hospital {

  private final UUID id;

  private final LocalDateTime createdDate;

  private final LocalDateTime lastModifiedDate;

  private static final OrganizationType organizationType = OrganizationType.HOSPITAL;

  @NonNull private final String name;
  @NonNull private final String address;
  @NonNull private final String telephoneNumber;
  @NonNull private final Integer totalNumberOfBeds;

  @NonNull @Builder.Default private final Set<Long> hospitalUsersIds;
}
